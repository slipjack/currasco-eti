
# EtI Curriculum - TEST

Steps to use the [`currasco` commandline interface (CLI)](https://gitlab.com/ttc/currasco) to pull the (public) [`currasco-eti`](https://gitlab.com/slipjack/currasco-eti) content and build it locally.

## Install dependencies

1. Make sure you have at least read access to the [`currasco`](https://gitlab.com/ttc/currasco) project 
2. Install debian dependencies with the following:
```
sudo apt install build-essential libssl-dev pandoc atom
```
3. Install useful *atom* packages manually or by running the following:
```
apm install markdown-preview-enhanced git-plus atom-mermaid intentions busy-signal linter linter-ui-default linter-remark atom-markdown-table-editor
```

## Install and configure NVM

Run the following:

```
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" 
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
```

## Use NVM to install and select the correct version of nodejs

Open a **new Terminal** and run the following:

```
nvm install v8.9.0
nvm use v8.9.0
```

## Install node dependencies for, and remove old versions of, currasco

Run the following:

```
npm install -g npm
npm install -g docsmith@beta
npm install -g react-native-scripts
npm uninstall -g currasco
```

## Globally install currasco using npm

Run the following: 

```
npm install -g currasco
```

## Create and enter a workspace directory

Run the following: 

```
cd ~/src
mkdir currascotest
cd currascotest
```

## Clear out old workspaces

Run the following: 

```
rm -rf @currasco
rm -rf ~/.content/*
```

## Initialize content-as-code and pull the currasco-eti content package

Run the following: 

```
currasco init
```

## Allow react-native to monitor lots of files (for mobile build on Linux)

Run the following: 

```
sudo sysctl -w fs.inotify.max_user_instances=524288
sudo sysctl -w fs.inotify.max_user_watches=5242880
#
### Do not run the following: 
# sudo sysctl -w fs.inotify.max_user_instances=1024
# sudo sysctl -w fs.inotify.max_user_watches=12288
#
### Or the following:
# sudo sysctl -w kern.maxfilesperproc=524288
# sudo sysctl -w kern.maxfiles=5242880
```

## Build local Web ("preview") and mobile versions

Run the following: 

```
currasco start
```

## View the Web version

Browse to `127.0.0.1:8081` in your browser

## View the mobile version

1. Make sure your mobile device is on the same local network as your development machine
2. Make sure the firewall on your development machine will allow incoming connections on ports `19000` and `19001`
3. Install `expo` on your mobile device ([[Android](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=en_US)] [[iOS](https://itunes.apple.com/us/app/expo-client/id982107779)])
4. Launch `expo`
5. Click **Scan QR Code**
6. Scan the QR code that appears on your *Terminal* window
7. When the packager is done, the mobile app should appear on your mobile device

## Edit content and test live-reload

1. Launch *atom* and open one of the following files: 
    - `~/src/currascotest/@currasco/preview/index.md`
    - `~/src/currascotest/@currasco/preview/test-folder/index.md`
2. Edit the file and save your changes
3. Note the linting feedback in *atom*
4. Watch the web preview in your browser
5. Watch the mobile version on your device

