# EtI Curriculum

Investigative skills curriculum for [Exposing-the-Invisible](https://exposingtheinvisible.org)

---

**WORK IN PROGRESS - PLEASE DO NOT RELY ON THIS MATERIAL**

---

## Workshops

The following *workshops* contain session-specific material and *activities* such as those listed in the section below.

### [Using VPNs when investigating sensitive leaks](workshops/using-vpns-when-investigating-sensitive-leaks)

:[](workshops/using-vpns-when-investigating-sensitive-leaks#description)
:[](workshops/using-vpns-when-investigating-sensitive-leaks#overview)
:[](workshops/using-vpns-when-investigating-sensitive-leaks#duration)
:[](workshops/using-vpns-when-investigating-sensitive-leaks#learning-objectives)
:[](workshops/using-vpns-when-investigating-sensitive-leaks#prior-knowledge)

### [Using Tor when investigating sensitive leaks](workshops/using-tor-when-investigating-sensitive-leaks)

:[](workshops/using-tor-when-investigating-sensitive-leaks#description)
:[](workshops/using-tor-when-investigating-sensitive-leaks#overview)
:[](workshops/using-tor-when-investigating-sensitive-leaks#duration)
:[](workshops/using-tor-when-investigating-sensitive-leaks#learning-objectives)
:[](workshops/using-tor-when-investigating-sensitive-leaks#prior-knowledge)

### [Using Onion Services when investigating sensitive leaks](workshops/using-onion-services-when-investigating-sensitive-leaks)

:[](workshops/using-onion-services-when-investigating-sensitive-leaks#description)
:[](workshops/using-onion-services-when-investigating-sensitive-leaks#overview)
:[](workshops/using-onion-services-when-investigating-sensitive-leaks#duration)
:[](workshops/using-onion-services-when-investigating-sensitive-leaks#learning-objectives)
:[](workshops/using-onion-services-when-investigating-sensitive-leaks#prior-knowledge)

### [Using Tails When investigating sensitive leaks](workshops/using-tails-when-investigating-sensitive-leaks)

:[](workshops/using-tails-when-investigating-sensitive-leaks#description)
:[](workshops/using-tails-when-investigating-sensitive-leaks#overview)
:[](workshops/using-tails-when-investigating-sensitive-leaks#duration)
:[](workshops/using-tails-when-investigating-sensitive-leaks#learning-objectives)
:[](workshops/using-tails-when-investigating-sensitive-leaks#prior-knowledge)

---

## Activities

The following *activities* can be used for standalone sessions or as part of broader *workshops* such as those above.

### [How the Internet works](activities/how-the-internet-works)

:[](activities/how-the-internet-works#description)
:[](activities/how-the-internet-works#duration)
:[](activities/how-the-internet-works##learning-objectives)

### [Hands-on with the RiseUp VPN](activities/hands-on-with-riseup-vpn)

:[](activities/hands-on-with-riseup-vpn#description)
:[](activities/hands-on-with-riseup-vpn#overview)
:[](activities/hands-on-with-riseup-vpn#duration)
:[](activities/hands-on-with-riseup-vpn##learning-objectives)
:[](activities/hands-on-with-riseup-vpn#prior-knowledge)

### [HTTPS and Tor](activities/https-and-tor)

:[](activities/https-and-tor#description)
:[](activities/https-and-tor#duration)
:[](activities/https-and-tor##learning-objectives)

### [Investigative resources and Tor](activities/investigative-resources-and-tor)

:[](activities/investigative-resources-and-tor#description)
:[](activities/investigative-resources-and-tor#overview)
:[](activities/investigative-resources-and-tor#duration)
:[](activities/investigative-resources-and-tor##learning-objectives)
:[](activities/investigative-resources-and-tor#prior-knowledge)

### [Hands-on with the Tor Browser](activities/hands-on-with-tor-browser)

:[](activities/hands-on-with-tor-browser#description)
:[](activities/hands-on-with-tor-browser#overview)
:[](activities/hands-on-with-tor-browser#duration)
:[](activities/hands-on-with-tor-browser##learning-objectives)
:[](activities/hands-on-with-tor-browser#prior-knowledge)

### [Your mobile phone](activities/your-mobile-phone)

:[](activities/your-mobile-phone#description)
:[](activities/your-mobile-phone#duration)
:[](activities/your-mobile-phone##learning-objectives)

### [How mobile communication works](activities/how-mobile-communication-works)

:[](activities/how-mobile-communication-works#description)
:[](activities/how-mobile-communication-works#duration)
:[](activities/how-mobile-communication-works##learning-objectives)

### [Hands-on with OrBot and Onion Browser](activities/hands-on-with-orbot-and-onion-browser)

:[](activities/hands-on-with-orbot-and-onion-browser#description)
:[](activities/hands-on-with-orbot-and-onion-browser#overview)
:[](activities/hands-on-with-orbot-and-onion-browser#duration)
:[](activities/hands-on-with-orbot-and-onion-browser##learning-objectives)
:[](activities/hands-on-with-orbot-and-onion-browser#prior-knowledge)

