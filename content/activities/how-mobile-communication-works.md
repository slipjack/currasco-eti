---
title: How Mobile Communication Works
uuid: d89cab05-b823-4884-9377-071125ce51e2
locale: en
source: Tactical Tech
item: Activities
tags:
  - Mobile
duration: 60
description: See how mobile communication works, and what information third parties have access to along the way.
---

# How Mobile Communication Works

## Meta information

### Description
See how mobile communication works, and what information third parties have access to along the way.


### Duration
45 - 60 minutes.


### Ideal Number of Participants
This session can be done with any number of participants.


### Learning Objectives
##### Knowledge
- Understand the infrastructure of mobile communication, and who owns which parts of this infrastructure
- See where vulnerabilities lie: who can, or could, access your communication or data at which points?
- Understand the differences between cellular networks (including 3G/4G) and Wi-Fi

##### Attitude
- Mobile communication infrastructure is a physical thing, and there are choices you can make about how securely your data travels through it.


### Materials and Equipment Needed
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)
- @[material](412c1eb9-9fc6-4f6b-975a-6d7c1915f750)
- @[material](16c01d17-9ba7-47d6-815a-75cf9633004f)
- @[material](b6be8eed-7382-4594-bbe1-eaf471f8f081)
- @[material](9c584197-c4e1-4128-b2cf-60f91e47d52d)
- @[material](9824a3dd-12ed-4684-9afa-dd75118404ee)


## Activity

#### Preparation
1. Choose whether you are going to explain 'How mobile communication works' using (1) _Mobile Communication Cards_ or using (2) paper and pen.
2. For either option, be prepared to demonstrate how mobile communication works in at least two scenarios:
    - through cellular networks (voice/sms, chat apps)
    - through Wi-Fi (eg chat apps or browsing)
3. If not already covered, also be prepared to draw the infrastructure of the internet, including how it works when using a VPN, proxy, and Tor. It could be useful to have a diagram at hand as a guide.
4. *Cards*: If you're running the Cards version of this activity, print out ready-made cards from the [materials page on MyShadow.org](https://myshadow.org/materials), or create your own. You will need at least two sets: Mobile Infrastructure cards, and Internet Infrastructure cards. If the group if big, you may need to print out multiple sets. Ideally there should be one card per person.
5. *Paper and pen:* If you are running this activity without cards, Make sure you have enough paper, pens and markers. There should be multiple pieces of paper per participant.

### Option 1 - CARDS (40 min)

#### What does the cellular network infrastructure look like?
1. Depending on numbers, this exercise can be done in one group or multiple groups. Give each group one set of cards and ask them to put the cards in the correct order, showing how a mobile phone connects to another mobile phone through a cellular network.
2. Compare results and then go through the order together. Ask participants if there are specific concepts that are unclear, and clarify.

#### How does a mobile phone connect to the internet?
Using the cards, get participants to demonstrate the difference between (1) connecting to the internet through a cellular network - cell phone towers, 3G/4G - and (2) connecting through Wi-Fi.
1. What parts of the mobile phone and mobile communications infrastructure are involved?
2. Who has, or could have, access to what information, at which points along the way?


### Option 2 - PAPER AND PEN (40 min)

#### Draw the cellular network infrastructure
1. Hand out paper and pens to each participant, and write up some key words on the flip chart: cell phone tower, 3G/4G, telco, triangulation.
2. Ask participants to draw what the mobile communications infrastructure looks like, showing how a text message (SMS) travels from their phone to a friend's phone.
3. Break participants into small groups. to present their drawings to each other and discuss differences and similarities.
4. On the flipchart, draw how how mobile communication works. This should include the baseband, 3G/4G, Cell phone towers, mobile phone provider, triangulation.

#### Draw how a mobile phone connects to the internet
1. Ask participants to get into small groups and together draw how a mobile phone connects to the internet (give a specific case study, eg 'connecting to a website')
2. Groups come back together, and compare their drawings.
3. On the flipchart, draw two ways in which a mobile phone can connect to the internet:
    - Through the baseband, 3G/4G, Cell phone towers
    - Through Wi-Fi Receiver, Router, internet provider, internet infrastructure.
4. Discuss: Who has, or could have, access to what information, at which points along the way?

### Key concepts to cover
1. Mobile phone infrastructure and triangulation
2. Internet infrastructure
3. The difference between connecting over 3G/4G (cellular networks) and Wi-Fi (internet).

