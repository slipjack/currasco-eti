---
title: Hands-on with the RiseUp VPN
uuid: 8896e4fe-205d-474d-8064-051aa2e03c0d
locale: en
source: Tactical Tech
item: Activities
tags:
  - vpn
  - hands-on
  - digital security
duration: 90
description: "Participants will install and configure RiseUp's 'Red' Virtual Private Network (VPN)"
---

# Hands-on with the RiseUp VPN

## Meta information

### Description

Participants will install and configure RiseUp's "Red" Virtual Private Network (VPN)

### Overview

1. Participants create RiseUp accounts
2. Participants install OpenVPN clients and configure them to work with RiseUp's "Red" VPN service
3. Use the EFF's *Panopticlick* site to discuss *browser fingerprinting* 
4. Demonstrate the use of a VPN when accessing a few online investigative resources
  
### Duration

90 minutes

### Learning objectives

Participants should 

- Learn how to create an account on the RiseUp email and VPN service
- Learn how to create RiseUp invite codes for others
- Learn how to install and configure the RiseUp VPN
- Gain an understanding of *browser fingerprints* and the limited anonymity provided by VPNs
- Practice using a VPN to visit online investigative resources

### Prior Knowledge

- RiseUp's explanation of [How VPNs work](https://riseup.net/en/vpn/how-vpn-works)
- *Step 2: Explaining VPNs* from *Using VPNs when investigating sensitive leaks*

### Materials and Equipment Needed

- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](e96c589f-f1c5-49de-8493-ca39de05a502)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](e23c8b6f-863a-4c41-9a10-db764a0f8272)
- @[material](653ac493-71b0-4e17-8c61-bab08adbfda6)
- @[material](f6cd74bf-5a89-4fdc-8122-e305f947e14c)

### References

#### Background reading

- RiseUp's explanation of [Why you might want to use a VPN](https://riseup.net/vpn#why-would-you-want-to-use-the-riseup-vpn)
- RiseUp's explanation of [How VPNs work](https://riseup.net/en/vpn/how-vpn-works)

#### Useful resources for the session

- The RiseUp [account creation page](https://account.riseup.net/user/new)

- RiseUp VPN installation instructions
    - For [Windows](https://riseup.net/en/vpn/vpn-red/windows)
    - For [Mac](https://riseup.net/en/vpn/vpn-red/mac)
    - For [Linux](https://riseup.net/en/vpn/vpn-red/linux)

- Technical configuration details for the RiseUp VPN
    - [Configuration in a nutshell](https://riseup.net/en/vpn/vpn-red#configuration-in-a-nutshell)
    - [Example config file](https://riseup.net/en/vpn/vpn-red/riseup.ovpn)
    - [CA certificate](https://riseup.net/security/network-security/riseup-ca/RiseupCA.pem) (Right-click and select *Save link as*)
    - [Troubleshooting tips](https://riseup.net/en/vpn/vpn-red/troubleshooting)

- Test pages
    - The [What is my IP Address](https://whatismyipaddress.com) IP lookup service
    - The [I Can Haz IP](https://icanhazip.com) IP lookup service
    - The EFF's [Panopticlick](https://panopticlick.eff.org/) *browser fingerprint* test page 
    - The [Tin Eye](https://www.tineye.com) reverse image search engine
    - [Jeffrey's metadata viewer](http://exif.regex.info/exif.cgi)


## Activity

![](../../images/stop.svg){.smallimg}

### Preparation

1. Ensure that your local network does not block access to VPNs

2. Unless your Internet connection is very strong, consider distributing USB sticks containing **the current versions** of the OpenVPN client installers for Mac and Windows. Most people use a Mac client called *Tunnelblick*. (You can, of course, include software and data for other sessions, as well, to avoid having to collect and re-distribute the USB sticks between sessions or between days of a multi-day workshop.) Otherwise, have the download URLs ready on the projector:
    - **Tunnelblick (stable) for Mac**: https://www.tunnelblick.net/downloads.html
    - **OpenVPN (current) for Windows**: https://openvpn.net/index.php/open-source/downloads.html

3. Prepare a unique RiseUp *invite code* for each participant and include one on each USB stick in a `.txt` file. These invitations expire after a few months, so you should not create them too far in advance of the workshop

4. Determine ahead of time which operating systems are represented. If appropriate, ask participants to rearrange themselves according to OS. This will make it easier for the facilitator(s) (and other participants) to help those who get stuck.

5. Have the *Useful resources* links, above, ready to display on the projector

6. If OpenVPN is already installed on the computer you will be connecting to the projector, uninstall it completely before the session begins

### Key elements

1. Consider walking through the account creation, installation and configuration process on the projector. If possible, do so for whichever operating system is in use by the most participants

2. Ask those using less popular operating systems to sit together and have a co-facilitator who is familiar with those operating systems work closely with that group throughout the installation process. This does not always work, but, when it does, it can save you from having to repeat the process twice, switching computers or fiddling with virtual machines in the middle. 

3. Help participants create RiseUp accounts. Encourage the selection of non-sensitive, non-revealing usernames in case they *also* want to use RiseUp as a pseudonymous email account provider

4. Help participants install OpenVPN or Tunnelblick

5. Help participants configure OpenVPN to work with RiseUp's service

6. When everyone has OpenVPN up and running, ask them to visit the [What is my IP Address](https://whatismyipaddress.com) service and compare results with one another. Discuss how *IP lookup* services like this work. Visit the same site, on the projector, with your VPN disabled. 

7. On the projector, visit the EFF's [Panopticlick](https://panopticlick.eff.org/) page with your VPN disabled. Ask participants to do the same, both with and without the VPN. Discuss what it means for anonymity.

8. Ask participants to visit a few investigative resources with their VPN enabled and discuss what this means for the confidentiality of data and for their own potential anonymity:
    - **HTTPS**: The [Tin Eye](https://www.tineye.com) reverse image search engine
    - **HTTP**: [Jeffrey's metadata viewer](http://exif.regex.info/exif.cgi)

### Challenges and opportunities

- The process of installing and configuring the OpenVPN client can be quite time consuming, particularly in a session with two or three different operating systems. It is a good idea to plan for this. If you are running out of time and are unable to get it working for a few participants, consider having them install the Tor Browser instead. (Assuming you do not already have a Tor Browser session planned.) It is much easier. That way, they will be able to follow along with the rest of the session. If you do this, however, you should be prepared to give a very quick explanation of how Tor works. (See *Step 2: Explaining Tor* of the *Using Tor when investigating sensitive leaks* workshop.)
- If you do not use Windows, consider installing and practicing with a *VirtualBox* virtual machine running Windows 10. The same applies to Ubuntu Linux. (Unfortunately, Mac OS X virtual machines are hard to come by.) When following along with complicated installations, it is often helpful to be able to use the "majority" OS rather than your own.
- On some operating systems, clicking on a link to a `.pem` file will attempt to install the CA. Instead, participants can right-click and select *Save link as* (or the equivalent for their browser)
- Participants on Windows may need to run the OpenVPN client as *Administrator*. They can do this by right-clicking the application and selecting *Run as Administrator*.

### Warnings

- VPNs sometimes disconnect, and they tend to "fail open." This means that, unless you manually check the status of your VPN from time to time, you may end up spending hours 

### Common questions

- **What about mobile devices?** There are a number of mobile VPNs, but it can be difficult to find one that is free, opensource and based on OpenVPN. 
- **Can I use a VPN with Tor?** It is possible, but difficult. More importantly, it *reduces* the anonymity provided by the Tor Browser. It is occasionally useful to connect *first* to Tor, *then* to a VPN, but only as a way to visit websites that block access from Tor. (This is particularly useful on Tails, were *all* traffic is routed through Tor.)

