---
title: Investigative resources and Tor
uuid: 48723fe1-d1a8-48ce-a5c2-a3e53781b943
locale: en
source: Tactical Tech
item: Activities
tags:
  - tor
  - website investigation
  - verification
  - digital security
duration: 30
description: "Participants will explore various online investigative resources through Tor."
---

# Investigative resources and Tor

## Meta information

### Description

Participants will explore various online investigative resources through Tor.

### Overview

1. Ask participants to brainstorm online investigative resources, visit them using the Tor Browser and discuss challenges and workarounds
2. Ask participants to brainstorm leaks databases, visit them using the Tor Browser and download some content

### Duration

30 minutes

### Learning objectives

Participants should 

- Learn how to work around some of the challenges they will encounter while using Tor for investigative work
- Gain an appreciation for the risks associated being seen to download certain content or research particular topics and begin developing a sense of the extent to which Tor can help mitigate those risks

### Prior Knowledge

- *Step 2: Explaining Tor* of *Using Tor when investigating sensitive leaks*
- *Hands-on with Tor* or *Hands-on with OrBot and Onion Browser*

### Materials and Equipment Needed

- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](e96c589f-f1c5-49de-8493-ca39de05a502)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)

### References


#### Useful resources for the session

- The [Flight Radar](https://www.flightradar24.com) flight tracking service

- The [Tin Eye](https://www.tineye.com) reverse image search engine

- From the *[Leak and Onion Soup](https://exposingtheinvisible.org/guides/leak-and-onion-soup)* guide on *Exposing the Invisible*: 
  - The Introduction 
  - The *[Tor](https://exposingtheinvisible.org/guides/leak-and-onion-soup#tor)* section

## Activity

### Preparation 

1. Participants should already have Tor installed (either on a computer or a mobile device) and be comfortable using it
2. Be ready to display the following tables on the projector, but do not do so until groups have had a chance to brainstorm their own lists. ([The authoritative versions of these tables](https://exposingtheinvisible.org/guides/leak-and-onion-soup#tor-compatibility) are in the *Leak and Onion Soup* guide on Exposing the Invisible.)


### Online investigative resources and Tor compatibility

The table below notes the level of Tor compatibility (*cap* = CAPTCHA required) and presence of HTTPS encryption for a few investigative resources.

| Archive                        | Description                                                      | HTTPS | Tor |
| ------------------------------ | ---------------------------------------------------------------- | ----- | --- |
| [DuckDuckGo][29]                | Privacy aware search engine                                      | yes   | yes |
| [Tin Eye][25]                   | Reverse image search  **(Blocks Tor users)**                     | yes   | no  |
| [AI's YouTube data viewer][33]  | Reverse image search for video frames **(Broken HTTPS)**         | yes   | yes |
| [Picodash][28]                  | Time- and location-based Instagram search. **($8/mo)**           | yes   | yes |
| [Jeffrey's metadata viewer][35] | Displays embedded metadata in images and other documents         | no    | cap |
| [Foto Forensics "Lab"][27]      | Find evidence of tampering in image files **($5/10 images)**     | yes   | no  |
| [WHOIS lookup][31]              | Domain name ownership database                                   | yes   | cap |
| [Opencorporates][37]            | Public information on companies. **(Blocks Tor users)**          | yes   | no  |
| [Opencorporates viz][32]        | Network visualisation of various financial companies             | yes   | yes |
| [Flight radar][24]              | Live aircraft tracking. **(Sattelite view only through Tor)**    | yes   | cap |
| [Google][36]                    | Search engine. **(CAPTCHAs are often unsolvable Tor)**           | yes   | cap |
| [Google Translate][34]          | Machine translation of text                                      | yes   | cap |
| [Google Images search][26]      | Find images by uploading a similar image (or URL)                | yes   | cap |
| [YouTube][38]                   | Post or search for videos on YouTube                             | yes   | yes |


### Example leak repositories

Unlike investigative "cloud services," such as those listed above, repositories of leaked data almost never block access through through Tor. The table below includes a few examples.


| Archive               | Date range  | Type                | Platform       | Size                     | Tor |
| --------------------- | ----------- | ------------------- | -------------- | -------------------------| --- |
| [Swiss HSBC leaks][16] | to 2007     | Excerpts            | ICIJ website   | Based on 60k docs        | yes |
| [Offshore Leaks][17]   | to 2007     | Searchable archive  | ICIJ website   | 500k company profiles    | yes |
| [Offshore Leaks DB][18]| to 2007     | [csv][19],[neo4j][20] | ICIJ website   | 500k company profiles    | yes |
| [Cablegate][11]        | to 2010     | Searchable archive  | Wikileaks      | 250k cables              | yes |
| [Stratfor leaks][14]   | to Dec 2011 | Searchable archive  | Wikileaks      | 5M emails                | yes |
| [Clinton emails][10]   | to Aug 2014 | Searchable archive  | Wikileaks      | 30k emails               | yes |
| [Vault 7 projects][22] | to 2015     | Exceprts            | Wikileaks      | 50 documents             | yes |
| [Panama Papers][12]    | to 2015     | Excerpts            | DocumentCloud  | 150 of 11.5M docs        | yes |
| [HackingTeam leaks][15]| to Jul 2015 | Searchable archive  | Wikileaks      | 1 million emails         | yes |
| [Vault 7][23]          | to Feb 2016 | Archive             | Wikileaks      | 8k pages, 1k docs        | yes |
| [DNC leaks][21]        | to May 2016 | Searchable archive  | Wikileaks      | 44k emails, 17k docs     | yes |

### Key elements

1. Split participants into groups of three to five

2. Ask each group to brainstorm two lists and record them on flipchart paper:
    - Websites containing sensitive leaks (or other content) that they might be nervous about visiting — or about *being known to have visited*.
    - Websites that are useful investigative resources

3. Put the tables above on the projector, along with [a link to the online version](https://exposingtheinvisible.org/guides/leak-and-onion-soup#tor-compatibility) so participants can click the links

4. Ask participants to visit a few websites from the investigative resources category using Tor. They can do so individually or as a group, but each group should add to their list of sites: 
    - Interesting discoveries
    - Highest number of solved CAPTCHAs required to gain access
    - Other Tor-related challenges
    - Any workarounds they came up with for addressing those challenges

5. Ask each participant to download some data from one of the leaks databases. This might be:
    - A `.csv` dataset
    - A `.pdf`
    - A spreadsheet or text document
    - Interesting Web content copied and pasted from the browser

6. Ask each group to report back on two to three take-aways from the exercise. Participants should also display their lists on the walls, if possible, so that others can learn from them.

7. To wrap-up: 
    - Discuss the activity and answer questions
    - Propose known workarounds that were not discovered by participants
    - Ensure that the *Warnings* below have been mentioned
    - (Optional) Mention the possibility of (**and the risks associated with**) routing a VPN over Tor as discussed in the *[Leak and Onion Soup](https://exposingtheinvisible.org/guides/leak-and-onion-soup)* guide on *Exposing the Invisible* (and in the *Using Tails when investigating sensitive leaks* workshop).  
  
### Warnings

- The Tor Browser can not hide your location from code embedded in files (like PDFs and Word documents) if they are opened outside of the Tor Browser. 

- *Exposing the Invisible* makes no claim as to the validity or usefulness of the leak archives above. At the very least, however, they include *lots of data* that you can use to practice analysing leaked emails and documents. Just remember, if you do not want to risk walking around with illegally disclosed files on your laptop, consider the following:
  1. Make sure you have a handle on the level of risk associated with using Tor wherever you are,
  2. Subject to the above, use Tails when searching or downloading leaked content,
  3. Choose a strong passphrase when you configure *Persistence* on your Tails USB stick, and
  4. Think twice before crossing a border with that Tails USB stick.


[10]: https://wikileaks.org/clinton-emails/
[11]: https://wikileaks.org/plusd/?qproject[]=cg&q=
[12]: https://www.documentcloud.org/public/search/Source:%20%22Internal%20documents%20from%20Mossack%20Fonseca%20%28Panama%20Papers%29%22
[14]: https://search.wikileaks.org/gifiles/
[15]: https://wikileaks.org/hackingteam/emails/
[16]: https://www.icij.org/project/swiss-leaks/explore-swiss-leaks-data
[17]: https://offshoreleaks.icij.org/
[18]: https://offshoreleaks.icij.org/pages/database
[19]: https://offshoreleaks-data.icij.org/offshoreleaks/data-csv.zip
[20]: https://offshoreleaks-data.icij.org/offshoreleaks/neo4j/panama-papers-windows-2017-04-18.zip
[21]: https://wikileaks.org/dnc-emails/
[22]: https://wikileaks.org/vault7/document/
[23]: https://wikileaks.org/ciav7p1/cms/index.html
[24]: https://www.flightradar24.com
[25]: https://www.tineye.com
[26]: https://images.google.com
[27]: https://lab.fotoforensics.com
[28]: https://www.picodash.com/
[29]: https://duckduckgo.com/
[30]: https://panamapapers.icij.org/
[31]: https://whois.icann.org/en
[32]: https://opencorporates.com/viz/financial/index.html
[33]: https://www.amnestyusa.org/sites/default/custom-scripts/citizenevidence/
[34]: https://translate.google.com
[35]: http://exif.regex.info/exif.cgi
[36]: https://www.google.com
[37]: https://opencorporates.com/
[38]: https://www.youtube.com/watch?v=NAvlZQ9Y0DI

