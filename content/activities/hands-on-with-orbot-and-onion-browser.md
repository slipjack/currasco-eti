---
title: Hands-on with OrBot and OnionBrowser
uuid: def01f8e-b28e-443e-9970-c2e8613e1931
locale: en
source: Tactical Tech
item: Activities
duration: 30
description: "Participants will install OrBot (Android) or Onion Browser (iOS) on their mobile devices, verify that they are working, and use them to visit a few sites anonymously"
---

# Hands-on with OrBot and OnionBrowser

## Meta information

### Description

Participants will install OrBot (Android) or Onion Browser (iOS) on their mobile devices, verify that they are working, and use them to visit a few websites anonymously

### Overview

1. Help participants install the Tor Browser on their computers and verify that it is working
2. Use the EFF's *Panopticlick* site to discuss *browser fingerprinting* 
3. (optional) Demonstrate how the Tor Browser chooses new circuits for each site and changes them over time
4. Demonstrate one or two online investigative resources that are challenging to use through Tor

### Duration

30 minutes

### Learning objectives

Participants should 

- Learn how to install and use OrBot or the Onion Browser
- Learn how to verify that OrBot or the Onion Browser is working
- Gain a better understanding of *browser fingerprints* and explore the extent to OrBot and the Onion Browser hide them
- Gain an appreciation for some of the challenges they will face when doing investigative research through Tor 

### Prior Knowledge

- *Step 2: Explaining Tor* of *Using Tor when investigating sensitive leaks*
- *Hands-on with Tor*

### Materials and Equipment Needed

- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](6d758ada-e6cf-4a56-a96b-f84dfe14181c)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)

### References


#### Useful resources for the session

- [OrBot](https://play.google.com/store/apps/details?id=org.torproject.android) on Google's Play Store for Android
- [OrFox](https://play.google.com/store/apps/details?id=info.guardianproject.orfox) on Google's Play Store for Android
- [Onion Browser](https://itunes.apple.com/us/app/onion-browser-secure-anonymous-web-with-tor/id519296448?mt=8) on the Apple's App Store for iOS
- The [Tor Check](https://check.torproject.org) Tor verification service
- The [What is my IP Address](https://whatismyipaddress.com) IP lookup service
- The EFF's [Panopticlick](https://panopticlick.eff.org/) *browser fingerprint* test page 
- The [Flight Radar](https://www.flightradar24.com) flight tracking service
- The [Tin Eye](https://www.tineye.com) reverse image search engine

## Activity

![](../../images/stop.svg){.smallimg}

### Preparation

1. If possible, ask participants to rearrange themselves in two groups: one for Android devices and one for iOS devices. This will make it easier for the facilitator(s) (and other participants) to help those who get stuck. 
2. Confirm that the local network does not block access to Tor. If it does, see if it works with a [Bridge relay](https://www.torproject.org/docs/bridges) and consider distributing one or more bridge addresses to participants, either by printing them out or by putting them in a text file on the USB sticks that you distribute. (Once connected, participants should lookup additional bridge addresses so they do not all continue using the same one).

### Key elements

1. When everyone has the Tor Browser up and running, ask them to visit [Tor check](https://check.torproject.org) and [What is my IP Address](https://whatismyipaddress.com) websites and compare results with one another. Discuss how these services work. Ask them to visit the same websites while *not* using the Tor Browser and compare results. 
2. Compare the IP addresses presented by the two *IP Lookup* websites and use it as an opportunity to discuss *circuit isolation*. 
3. Ask participants to visit the EFF's [Panopticlick](https://panopticlick.eff.org/) page, both using Tor and their regular mobile browser. Ask participants to do the same and discuss what it means. 
4. Use a few investigative resources to demonstrate some of the challenges that come along with using Tor: 
    - The [Tin Eye](https://www.tineye.com) reverse image search engine does not allow uploads from Tor
    - The [Flight Radar](https://www.flightradar24.com) flight tracking service requires Tor visitors to solve CAPTCHAs. It may also show a blank map initially, but work fine in Satellite view. This kind of experimentation (and patience) is often necessary if you need to use Tor regularly.
5. (Optional) If enough time has passed, reload [What is my IP Address](https://whatismyipaddress.com) in a new tab and see if it displays a new IP address. Discuss the fact that even long-lived connections to some websites will eventually choose new circuits.

### Challenges and opportunities

- Smartphone app installation is generally quite simple, whereas connecting a mobile device to a projector can be a quite a challenge. As a reuslt, you will probably just want to give participants time to complete the installation while walking around and helping anyone who gets stuck. 
- If you *are* able to connect a mobile device to your projector, consider following along with the key elements above
- Android users will need to install *both* OrBot and OrFox.
- Discussions about mobile (in)security have a tendency to sprawl. Be prepared either to limit the discussion or to offer a full session on the topic. (See the *Mobile Communications* Workshop.) Keep in mind, however, that a full session might take two hours or longer.

### Warnings

- Discuss the ways in which the nature of mobile devices (always on, location tracking, embedded microphones, baseband communications, etc.) limit the level of anonymity these apps can provide
- Other apps will not use Tor just because Tor (or a Tor-compatible browser) is running.
- Give time at the end of the session for participants who are worried about having the Tor Browser installed on their devices to remove it

