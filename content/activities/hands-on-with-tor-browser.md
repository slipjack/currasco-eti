---
title: Hands-on with the Tor Browser
uuid: 83da798d-1f6c-4d32-954d-77b970494a37
locale: en
source: Tactical Tech
item: Activities
tags:
  - tor
  - hands-on
  - digital security
duration: 45
description: "Participants will install the Tor Browser on their computers, launch it and visit a few websites to demonstrate that it is working"
---

# Hands-on with the Tor Browser

## Meta information

### Description

Participants will install the Tor Browser on their computers, launch it and visit a few websites to demonstrate that it is working

### Overview

1. Help participants install the Tor Browser on their computers and verify that it is working
2. Use the EFF's *Panopticlick* site to discuss *browser fingerprinting* 
3. (optional) Demonstrate how the Tor Browser chooses new circuits for each site and changes them over time
4. Demonstrate one or two online investigative resources that are challenging to use through Tor
  
### Duration

45 minutes

### Learning objectives

Participants should 

- Learn how to install, launch and use the Tor Browser
- Learn how to verify that the Tor Browser is working
- Gain a better understanding of *browser fingerprints* and how the Tor Browser hides them
- Gain an appreciation for some of the challenges they will face when doing investigative research through Tor 

### Prior Knowledge

- *Step 2: Explaining Tor* of *Using Tor when investigating sensitive leaks*

### Materials and Equipment Needed

- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](e96c589f-f1c5-49de-8493-ca39de05a502)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](e23c8b6f-863a-4c41-9a10-db764a0f8272)

### References


#### Useful resources for the session

- The [Tor Browser download page](https://www.torproject.org/download/download.html.en) for all operating systems

- The [Tor Check](https://check.torproject.org) Tor verification service

- The [What is my IP Address](https://whatismyipaddress.com) IP lookup service

- The EFF's [Panopticlick](https://panopticlick.eff.org/) *browser fingerprint* test page 

- The [Flight Radar](https://www.flightradar24.com) flight tracking service

- The [Tin Eye](https://www.tineye.com) reverse image search engine

- [Security-in-a-Box](https://securityinabox.org) Hands-on Guides for
  - [Windows](https://securityinabox.org/en/guide/torbrowser/win)
  - [Mac](https://securityinabox.org/en/guide/torbrowser/mac)
  - [Linux](https://securityinabox.org/en/guide/torbrowser/linux)

## Activity

![](../../images/stop.svg){.smallimg}

### Preparation

1. Unless your Internet connection is very strong, consider distributing USB sticks containing **the current versions** of the Tor Browser installer for Mac, Windows and Linux. (You can, of course, include software and data for other sessions, as well, to avoid having to collect and re-distribute the USB sticks between sessions or between days of a multi-day workshop.) Otherwise, have the download URL ready on the projector: `https://www.torproject.org/download/download-easy.html.en`
2. Determine ahead of time which operating systems are represented. If appropriate, ask participants to rearrange themselves according to OS. This will make it easier for the facilitator(s) (and other participants) to help those who get stuck.
3. Confirm that the local network does not block access to Tor. If it does, see if it works with a [Bridge relay](https://www.torproject.org/docs/bridges) and consider distributing one or more bridge addresses to participants, either by printing them out or by putting them in a text file on the USB sticks that you distribute. (Once connected, participants should lookup additional bridge addresses so they do not all continue using the same one).
4. Have the *Useful resources* links, above, ready to display on the projector

### Key elements

1. Ask participants to install the Tor Browser

2. When everyone has the Tor Browser up and running, ask them to visit [Tor check](https://check.torproject.org) and [What is my IP Address](https://whatismyipaddress.com) websites and compare results with one another. Discuss how these services work. Ask them to visit the same websites while *not* using the Tor Browser and compare results. 

3. Compare the (hopefully different) IP addresses presented by the two websites and use it as an opportunity to discuss *circuit isolation*. Close the *What is my IP address* tab on the projector.

4. On the projector, visit the EFF's [Panopticlick](https://panopticlick.eff.org/) page, both using the Tor Browser and using a regular browser. Ask participants to do the same and discuss what it means. 

5. Use a few investigative resources to demonstrate some of the challenges that come along with using Tor: 
    - The [Tin Eye](https://www.tineye.com) reverse image search engine does not allow uploads from Tor
    - The [Flight Radar](https://www.flightradar24.com) flight tracking service requires Tor visitors to solve CAPTCHAs. It may also show a blank map initially, but work fine in Satellite view. This kind of experimentation (and patience) is often necessary if you need to use Tor regularly.

6. (Optional) If enough time has passed, reload [What is my IP Address](https://whatismyipaddress.com) in a new tab and see if it displays a new IP address. Discuss the fact that even long-lived connections to some websites will eventually choose new circuits.

### Challenges and opportunities

- The Tor Browser installation process is quite simple, so you might not have to step through it, on the projector, along with the participants. Unless the vast majority of participants are on the same OS (and lack experience installing software), consider just giving them time to complete the installation while walking around and helping anyone who gets stuck.
- After installing the Tor Browser on a Mac, expect a security warning, [as discussed in Security-in-a-Box](https://securityinabox.org/en/guide/torbrowser/mac/#launching-and-configuring-the-tor-browser), on first launch
- Download speeds can be quite slow when Dozens of participants try to pull down the installer, at the same time, over a weak Internet connection

### Warnings

- Other network applications will not use Tor just because the Tor Browser is running. (Unless you are using Tails!)
- Give time at the end of the session for participants who are worried about having the Tor Browser installed on their computers to remove it

### Common questions

- **What about mobile devices?** See *Hands-on with OrBot and Onion Browser*
- **Can I use a VPN with Tor?** It is possible, but difficult. More importantly, it *reduces* the anonymity provided by the Tor Browser. It is occasionally useful to connect *first* to Tor, *then* to a VPN, but only as a way to visit websites that block access from Tor. (This is particularly useful on Tails, were *all* traffic is routed through Tor.)
