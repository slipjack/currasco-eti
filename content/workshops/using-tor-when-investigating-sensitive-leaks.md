---
title: Using Tor when Investigating sensitive leaks
uuid: d812ad60-3f3c-4c58-8f7f-367bc52949d8
locale: en
source: Tactical Tech
item: Workshops
tags:
  - tor
  - website investigation
  - verification
  - digital security
duration: 225
description: "Using the Tor Browser to protect yourself from exposure while obtaining, verifying and investigating highly sensitive leaks and other data"
---

# Using Tor when Investigating sensitive leaks

## Meta information

### Description

Using the Tor Browser to protect yourself from exposure while obtaining, verifying and investigating highly sensitive leaks and other data

### Overview

1. Activity: How the Internet works
2. Explaining Tor
3. Activity: HTTPS and Tor
4. Hands-on: The Tor Browser
5. (Optional) Hands-on: OrBot (Android) and Onion Browser (iOS)
6. Activity: Investigative resources and Tor

### Duration

3 hours and 40 minutes (plus intro and breaks)

### Learning Objectives

Participants should leave with:

- A non-technical understanding of how information travels over the Internet
- A non-technical understanding of how HTTPS and Tor help protect that information
- An understanding of the difference between *data* and *meta-data*
- The ability to install and use the Tor Browser on Windows, Mac OSX or Linux
- (Optional) The ability to install and use *OrBot* on Android or the *Onion Browser* on iOS
- Some awareness of the limitations of Tor and the challenges associated with using it properly

### Prior Knowledge

No prior knowledge is needed

### Materials and Equipment Needed

- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](e96c589f-f1c5-49de-8493-ca39de05a502)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)
- @[material](b84f7bdd-5702-44c7-a6e3-2c242d7e8579)
- @[material](f6cd74bf-5a89-4fdc-8122-e305f947e14c)

- Optional
  - @[material](6d758ada-e6cf-4a56-a96b-f84dfe14181c)
  - @[material](e23c8b6f-863a-4c41-9a10-db764a0f8272)
  
  
### References

#### Background Reading

- From the *[Leak and Onion Soup](https://exposingtheinvisible.org/guides/leak-and-onion-soup)* guide on *Exposing the Invisible*: 
  - The Introduction 
  - The *[Tor](https://exposingtheinvisible.org/guides/leak-and-onion-soup#tor)* section

- The Tor Project's *[Want Tor to really work?](https://www.torproject.org/download/download.html.en#warning)* warning

- [Security-in-a-Box](https://securityinabox.org) Hands-on Guides:
  - [Windows](https://securityinabox.org/en/guide/torbrowser/win)
  - [Mac](https://securityinabox.org/en/guide/torbrowser/mac)
  - [Linux](https://securityinabox.org/en/guide/torbrowser/linux)

#### Useful resources for the session

- The Tor Project's *[Overview](https://www.torproject.org/about/overview.html.en)* page

- The EFF's *[How HTTPS and Tor Work Together to Protect Your Anonymity and Privacy](https://www.eff.org/pages/tor-and-https)* interactive diagram

- The EFF's [Panopticlick](https://panopticlick.eff.org/) *browser fingerprint* test page 

- The [Tor Browser download page](https://www.torproject.org/download/download.html.en) for all operating systems

- From the *[Leak and Onion Soup](https://exposingtheinvisible.org/guides/leak-and-onion-soup)* guide on Exposing the Invisible:
  - The *[Tor compatibility](https://exposingtheinvisible.org/guides/leak-and-onion-soup#tor-compatibility)* table

#### Useful Case Studies

- [Ed Snowden Taught me to Smuggle Secrets Past Incredible Danger. Now I Teach You.](https://theintercept.com/2014/10/28/smuggling-snowden-secrets) by Micah Lee

## Steps

### Step 1: How the Internet works (30 min)

Use the *Internet infrastructure cards* to carry out the HTTP and HTTPS variations of *How the Internet Works*, below. Modify the activity as follows: 

- Create *GET https://wikileaks.org/vault7* and *Content of sensitive leak* cards and use them instead of the *Email* card, to represent the traffic sent by, and returned to, the user

- Use an envelope to represent HTTPS encryption between the browser and the destination website

- If there is time to cover email as well, consider an example in which: 
  - A whistle-blower sends a leak to an investigator 
  - The two correspondents use different webmail services
  - They are in different countries
  
- And, if covering email, be sure to *take the message out of the envelope when it gets to the sender's webmail server*. After that (and these are all good points for discussion): 
  - It may or may not go back into an envelope between the sender's server and the recipient's server
  - If it does, it should be taken out of the envelope at the recipient's server
  - It may or may not go back into an envelope between the recipient's server and her browser

- Do not cover VPNs

- Do not (yet) cover Tor

:[](../activities/how-the-internet-works##activity)

### Step 2: Explaining Tor (45 min)

Use the [diagrams on the Tor Project's Overview page](https://www.torproject.org/about/overview.html.en#thesolution) (or [those in the Tor section of Leak and Onion Soup](https://exposingtheinvisible.org/guides/leak-and-onion-soup#a-basic-tor-connection) to explain the basics of how Tor works. Use the EFF's [Panopticlick](https://panopticlick.eff.org/) test page to explain the concept of a *browser fingerprint*.

#### Key points

- The Tor network is made up of volunteer relays running audited, free and open-source, cross-platform software that uses standards-based, end-to-end encryption and clever routing to protect your identity
- The Tor Browser does more than just send your traffic through Tor. It also hides your browser's unique *fingerprint*
- For each request, the Tor Browser chooses three relays more-or-less at random. This is called a *curcuit*. It then encrypts each Web request to the third relay in the circuit, then to the second, then to the first. It then delivers the whole thing to the first, which decrypts it and sends it to the second, which decrypts it and sends it to the third, which decrypts it and sends it to the target website. Responses follows the reverse path back to your browser, with the third relay assembling the (onion like...) layers of encryption in the reverse order.
- Your ISP and the first Tor relay know your IP address (where you are on the Internet) but not where you're going
- The third Tor relay and the target website knows that *somebody* is using Tor to request a particular page, but they do not know where that request originated
- None of the relays knows *both* who you are and what you're doing. (Nor do your ISP or the target website)

#### Warnings

- Your ISP, the first Tor relay and surveillance operations can all tell that you are using Tor. **Tor does not hide the fact that you are using Tor**. 
- While Tor encrypts your Web traffic up through the third relay, **it is still important to make sure that you only exchange sensitive information with HTTPS encrypted websites.** Otherwise, the Tor exit relay will have access to the content you are sending and receiving.

#### Optional activity

If there is time — and if participants remain unclear about the basics of how Tor works — use the *Internet infrastructure cards* to carry out the Tor variation of *How the Internet Works*, below. Use three different-sized envelopes to represent the encryption between Tor relays and to demonstrate how Tor limits the exposure of metadata (who is requesting what) such that even the relays themselves do not know who is requesting what page. (Include a fourth envelope for HTTPS encryption if appropriate.)

Additional modifications include:

- Most of those listed above under *Step 1: How the Internet works*
- Do not cover email
- Do not cover VPNs

:[](../activities/how-the-internet-works##activity)

### Step 3: HTTPS and Tor (30 min)

The following activity helps demonstrate why HTTPS remains important even while using Tor. 

Suggestions:
- In the interest of covering both data encryption and anonymity, consider discussing two different scenarios: 
  1. **Why Tor matters**: From within her newsroom, a journalist downloads sensitive data that was accidentally left exposed on the website of a company that is widely suspected of engaging in corrupt business practices
  2. **Why HTTPS also matters**: An investigator working in a country with a powerful surveillance apparatus uses Tor to upload leaked images to [a non-HTTPS web service](http://exif.regex.info/exif.cgi) to see if they contain useful meta-data.

- Toggling either the *Tor* button or the *HTTPS* button on and off quickly makes the difference between the two states easier to see

:[](../activities/https-and-tor##activity)


### Step 4: Hands-on with the Tor Browser (45 min)

The main purpose of this session is to get the Tor Browser installed on participants' computers and make sure that everyone knows how to launch it and use it to visit websites anonymously.

:[](../activities/hands-on-with-tor-browser##activity)

### Step 5: Hands-on with OrBot (Android) and Onion Browser (iOS) (30 min)

The main purpose of this session is to get the Tor Browser installed on participants' mobile devices and make sure that everyone knows how to launch it and use it to visit websites anonymously.

:[](../activities/hands-on-with-orbot-and-onion-browser##activity)

### Step 6: Investigative resources and Tor (30 min)

The following activity is designed to give participants a chance to practice dealing with some of the frustrations they will encounter while using Tor. In some cases, they 

:[](../activities/investigative-resources-and-tor##activity)

### Common questions

- **What about *the Dark Web*?** See *Using Onion Services when investigating sensitive leaks*
