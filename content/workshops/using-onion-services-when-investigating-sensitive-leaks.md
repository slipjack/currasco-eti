---
title: Using Onion Services when investigating sensitive leaks
uuid: 5697cfaf-d0ea-4403-818c-b116b4cb8393
locale: en
source: Tactical Tech
item: Workshops
tags:
  - tor
  - website investigation
  - verification
  - digital security
duration: 120
description: "Using Onion Services to protect yourself from exposure and share data with others while obtaining, verifying and investigating highly sensitive leaks and other data"
---

# Using Onion Services when investigating sensitive leaks

## Meta information

### Description

Using Onion Services to protect yourself from exposure and share data with others while obtaining, verifying and investigating highly sensitive leaks and other data

### Overview

1. Activity: Dipping a toe into *the Dark Web*
2. Whistle blowing platforms and other *Onion services*
3. Hands-on: OnionShare
4. Wrap-up

### Duration

2 hours

### Learning Objectives

`<...>`

### Prior Knowledge

- *Using Tor when investigating sensitive leaks*

### Materials and Equipment Needed

- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)

### References

#### Background Reading

- `<...>`

#### Useful resources for the session

- `<...>`

#### Useful Case Studies

1. `<...>`
2. `<...>`

## Steps

### Step 1: Dipping a toe into the Dark Web (?? min)

`<...>`

##### Activity

`<...>`

### Step 2: Whistle blowing platforms and other *Onion services* (?? min)

`<...>`

### Step 3: Hands-on with OnionShare (?? min)

`<...>` 

### Step 4: Wrap up (5 min)

Ask participants if there questions, concepts that are unclear or general observations, and provide them with external resources.

