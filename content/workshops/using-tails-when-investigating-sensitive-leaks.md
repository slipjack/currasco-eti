---
title: Using Tails when investigating sensitive leaks
uuid: 41550290-dd7a-492a-8b67-4a61f6db31dd
locale: en
source: Tactical Tech
item: Workshops
tags:
  - tails
  - website investigation
  - verification
  - digital security
duration: 360
description: "Using the Tails to protect yourself from exposure and mitigate the effects of potentially malicious files while obtaining, verifying and investigating highly sensitive leaks and other data"
---

# Using Tails when investigating sensitive leaks

## Meta information

### Description

Using Tails to protect yourself from exposure and potentially malicious files while obtaining, verifying and investigating highly sensitive leaks and other data

### Overview

1. Activity: The Reboot challenge
2. Explaining Tails
3. Hands-on: Enabling persistence, allowing admin access and creating Tails disks for others
4. Activity: Commandline basics
5. Persistent software installation on Tails
6. Routing a VPN over Tor - Why you shouldn't and when you just might
7. Hands-on: Configuring and using `vot.sh` to visit websites that block access from Tor
8. Wrap-up

### Duration

6 hours

### Learning Objectives

`<...>`

### Prior Knowledge

- *Using Tor when investigating sensitive leaks*
- *Using VPNs when investigating sensitive leaks*

### Materials and Equipment Needed

- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](e23c8b6f-863a-4c41-9a10-db764a0f8272)

### References

#### Background Reading

- [Leak and Onion Soup](https://exposingtheinvisible.org/guides/google-dorking/), Guide on Exposing the Invisible (Tactical Tech)

#### Useful resources for the session

- [Leak and Onion Soup](https://exposingtheinvisible.org/guides/google-dorking/), Guide on Exposing the Invisible (Tactical Tech)

#### Useful Case Studies

1. `<...>`
2. `<...>`

## Steps

### Step 1: The Reboot challenge

`<...>`

##### Activity

`<...>`

### Step 2: Explaining Tails (?? min)

`<...>`

### Step 3: Hands-on with Tails (?? min)

```
Notes: 

- Enabling persistence, 
- Allowing admin access, and 
- Creating Tails disks for others 
```

### Step 4: Commandline basics (?? min)

`<...>`

##### Activity

`<...>`

### Step 5: Persistent software installation on Tails (?? min)

`<...>`

### Step 6: Routing a VPN over Tor - Why you shouldn't and when you just might (?? min)

`<...>`

### Step 7: Hands-on with vot.sh (?? min)

```
Notes:

- Configuring and using vot.sh to visit websites that block access from Tor
```

### Step 8: Wrap up (?? min)

Ask participants if there questions, concepts that are unclear or general observations, and provide them with external resources.

