---
title: Using VPNs when investigating sensitive leaks
uuid: 3e886218-6a75-4f7b-891d-d1a326c9044a
locale: en
source: Tactical Tech
item: Workshops
tags:
  - vpn
  - website investigation
  - verification
  - digital security
duration: 135
description: "Using a Virtual Private Network (VPN) protect yourself from exposure while obtaining, verifying and investigating highly sensitive leaks and other data"
---

# Using VPNs when investigating sensitive leaks

## Meta information

### Description

Using a Virtual Private Network (VPN) protect yourself from exposure while obtaining, verifying and investigating highly sensitive leaks and other data

### Overview

1. Activity: How VPNs Work
2. Explaining VPNs
3. Hands-on: The RiseUp VPN service
4. Wrap-up

### Duration

2 hours and 15 minutes

### Learning Objectives

Participants should leave with:

- A non-technical understanding of how VPNs help protect the confidentiality of information and, to a limited extent, the anonymity of the user sending or receiving it
- An understanding of the difference between *data* and *meta-data*
- The ability to install and use the RiseUp VPN on Windows, Mac OSX or Linux

### Prior Knowledge

- *How the Internet Works* (Either HTTP/HTTPS variation)
  
### Materials and Equipment Needed

- @[material](653ac493-71b0-4e17-8c61-bab08adbfda6)
- @[material](e23c8b6f-863a-4c41-9a10-db764a0f8272)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](e96c589f-f1c5-49de-8493-ca39de05a502)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)
- @[material](b84f7bdd-5702-44c7-a6e3-2c242d7e8579)
- @[material](f6cd74bf-5a89-4fdc-8122-e305f947e14c)

### References

#### Background Reading

- RiseUp's explanation of [How VPNs work](https://riseup.net/en/vpn/how-vpn-works)
- RiseUp's explanation of [Why you might want to use a VPN](https://riseup.net/vpn#why-would-you-want-to-use-the-riseup-vpn)

#### Useful resources for the session

- The [Tin Eye](https://www.tineye.com) reverse image search engine
- [Jeffrey's metadata viewer](http://exif.regex.info/exif.cgi)

### Useful case studies

- A free VPN provider allegedly violates its own privacy policy:
    - [ZDnet story](http://www.zdnet.com/article/privacy-group-accuses-hotspot-shield-of-snooping-on-web-traffic/)
    - [FTC filing](https://www.documentcloud.org/documents/3914264-FTC-complaint-about-Hotspot-Shield.html/)

## Steps

### Step 1: How VPNs work (30 min)

Use the *Internet infrastructure cards* to carry out the VPN variation of *How the Internet Works*, below. Modify the activity as follows: 

- Create *POST https://www.tineye.com* and *POST http://exif.regex.info/exif.cgi* cards and use them instead of the *Email* card, to represent the traffic when uploading an image to the *Tin Eye* and *Jeffrey's metadata viewer* services respectively
- Assume the user is in a different country from these services
- Use an envelope to represent HTTPS encryption (where appropriate) between the browser and the destination website
- Use another (larger) envelope to represent the VPN's encryption between the browser and VPN server
- Do not cover email unless there is time and interest
- Do not cover Tor

Topics for discussion might include: 

- What an attacker on your local network (or your ISP) can see in terms of content
- What an attacker on your local network (or your ISP) can see in terms of what sites you are accessing
- What the target website can see
- What the VPN can see

:[](../activities/how-the-internet-works##activity)

### Step 2: Explaining VPNs (15 min)

The *How VPNs Work* activity, above, should cover most of what participants need to know. However, it might still be useful to clarify a few details, such as:

- The fact that *OpenVPN* is free and open-source software (FOSS), whereas other VPNs may not be

- The fact that VPNs do not protect your anonymity as strongly as Tor

- The importance of "trust" when choosing a VPN:
  - Unlike Tor, VPNs do not protect your anonymity (where you are and what you are going) *from themselves*
  - Non-HTTPS traffic sent through a VPN will be visible to the VPN service
  - Non-HTTPS traffic sent through a VPN will *not* be visible to attackers on your local network, your ISP, etc.

### Step 3: Hands-on with the RiseUp VPN service (90 min)

The main purpose of this session is to get an OpenVPN client installed on each participant's computers and configure it to work with RiseUp's VPN service.

:[](../activities/hands-on-with-riseup-vpn##activity)
